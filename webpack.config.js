const path = require('path')
const webpack = require('webpack')
const HtmlWebPackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const fs = require('fs');

const DEV_MODE = process.env.NODE_ENV === 'dev'

const plugins = [
  new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery'
  }),
  new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: '[name]/styles.css'
  })
];

const cssStyleLoader = DEV_MODE ? 'style-loader' : MiniCssExtractPlugin.loader;

const entries = (() => {
  var res = {};
  fs.readdirSync('./app/').forEach((value) => {
    res[value] = `./app/${value}/assets/index.js`;
    plugins.push(new HtmlWebPackPlugin({
      chunks: [value],
      template: `app/${value}/index.html`,
      filename: `${value}/index.html`
    }));
  });
  return res;
})();

module.exports = {
  devtool: DEV_MODE ? 'eval' : 'source-map',
  entry: entries,
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name]/bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.s?[ac]ss$/,
        use: [
          cssStyleLoader,
          'css-loader',
          // 'postcss-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(jpe?g|png|gif)$/,
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]'
        }
      }
    ]
  },
  plugins: plugins
}
