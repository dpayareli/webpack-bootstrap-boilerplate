# Webpack 4 Boilerplate

> Webpack 4 boilerplate with Babel, Bootstrap 4, jQuery and SCSS on board

## Setup

Install dependencies

```sh
$ npm install
```

## Scripts

#### Run the local webpack-dev-server with livereload and autocompile on [http://localhost:8080/](http://localhost:8080/)

```sh
$ npm run dev
```

#### Build the current application for deployment

```sh
$ npm run build
```
